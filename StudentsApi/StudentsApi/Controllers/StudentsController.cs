﻿using Marten;
using Microsoft.AspNetCore.Mvc;
using StudentsApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentsApi.Controllers
{
    // route and controller MUST
    [Route("api/students")]
    [Controller]
    public class StudentsController : Controller
    {
        // store it in static for better access, start as null.
        public static IDocumentStore session = null;

        // check if there is something in the DB, if not - create.
        public StudentsController()
        {
            if (session == null)
            {
                DatabaseConfigurate();
            }
        }

        // database connection string and other settings go here.
        private void DatabaseConfigurate()
        {
            // store in session
            session = DocumentStore.For(settings => {
                // connectionstring
                settings.Connection("host=localhost;database=StudentsApi_db;username=postgres;password=postgres");
                // generate table for student
                settings.Schema.For<Student>();
            }
        );
        }


        [HttpGet]
        public ActionResult<List<Student>> GetAll()
        {
            using (var setsession = session.OpenSession())
            {
                return setsession.Query<Student>().ToList();         
            }
        }

        [HttpGet(@"{Id}")]
       public ActionResult<Student> StudentInfo([FromRoute]int Id)
        {
            using (var setsession = session.LightweightSession())
            {
                var student = setsession.Load<Student>(Id);
                return student;
            }
        }

        // function for creating students and storing them in the db with marten
        [HttpGet("create")]
        public void Create([FromQuery] string firstName, [FromQuery] string lastName, [FromQuery] int age)
        {
            using (var getsession = session.LightweightSession())
            {
                // setting up student 
                var studentinfo = new Student(firstName, lastName, age);
                
                // storing the info to db with marten and saving changes
                getsession.Store(studentinfo);
                getsession.SaveChanges();
            }
        }


        // store a new student in the db 
        [HttpPost("createnew")]
        public void CreateNew([FromBody]Student student)
        {
            using(var createsession = session.LightweightSession())
            {
                createsession.Store(student);
                createsession.SaveChanges();
            }
        }

        // update the current student
        [HttpPut("update/{Id}")]
        public void UpdateStudent([FromRoute]int Id, [FromBody]Student student)
        {
            using(var updatesession = session.LightweightSession())
            {

                student.Id = Id;

                updatesession.Store(student);
                updatesession.SaveChanges();
            }
        }

        [HttpDelete("delete/{Id}")]
        public void DeleteStudent([FromRoute]int Id)
        {
            using (var deletesession = session.LightweightSession())
            {
                deletesession.DeleteWhere<Student>(stud => stud.Id == Id);
                deletesession.SaveChanges();
            }
        }

    }
}
